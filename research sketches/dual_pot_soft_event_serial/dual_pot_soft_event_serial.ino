#include <PinChangeInt.h>

#define button 7

PROGMEM const int potX = A0;    // select the input pin for the potentiometer
PROGMEM const int potY = A1;
PROGMEM const int potMax = 1023;
int calX, calY;                // Initial potentiometer values.
int anX, anY;
volatile bool report = false;

uint8_t latest_interrupted_pin;

void pin3func() {
  if(PCintPort::pinState) {
    Serial.print("Pin "); Serial.print(button, DEC); Serial.println("!");
    report = true;
  }
}

void setup() {
  pinMode(button, INPUT);  
  digitalWrite(button, HIGH);
  PCintPort::attachInterrupt(button, &pin3func, CHANGE);
  
  calX = analogRead(potX);
  calY = analogRead(potY);
  
  Serial.begin(115200);
}

void loop() {
  if(report) {
    anX = analogRead(potX);
    anY = analogRead(potY);
    if(anX > calX) {
      Serial.print("X: ");
      Serial.print(map(anX, calX, potMax, 0, 127), DEC);
    } else {
      Serial.print("X: ");
      Serial.print(map(anX, 0, calX, -127, 0), DEC);
    }
    if(anY > calY) {
      Serial.print(" Y: ");
      Serial.println(map(anY, calY, potMax, 0, 127), DEC);
    } else {
      Serial.print(" Y: ");
      Serial.println(map(anY, 0, calY, -127, 0), DEC);
    }
    report = false;
  }
}
